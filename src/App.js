import "./App.css";
import { BrowserRouter } from "react-router-dom";
import RoutePages from "./config/RoutePages";
import Header from "./layouts/MainHeader";
import Footer from "./layouts/MainFooter";

export default function App() {
  return (
    <>
      <BrowserRouter>
        <Header />
        <RoutePages />
        <Footer />
      </BrowserRouter>
    </>
  );
}

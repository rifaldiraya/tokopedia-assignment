import { gql } from "@apollo/client";

export const GET_POKEMONS = gql`
  query GetPokemons($first: Int!) {
    pokemons(first: $first) {
      id
      name
      maxHP
      maxCP
      image
      classification
      attacks {
        special {
          damage
        }
      }
      evolutions {
        name
        classification
      }
    }
  }
`;

export const GET_POKEMON = gql`
  query GetPokemon($id: String) {
    pokemon(id: $id) {
      name
      number
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      types
      maxHP
      resistant
      maxCP
      classification
      fleeRate
      weaknesses
      fleeRate
      types
      image
      evolutions {
        name
        classification
      }
      attacks {
        special {
          name
          type
          damage
        }
        fast {
          name
          type
          damage
        }
      }
    }
  }
`;

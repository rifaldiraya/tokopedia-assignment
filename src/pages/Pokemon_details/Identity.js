import React from "react";
import { Row, Col } from "antd";

export default function Identity(props) {
  return (
    <>
      <Col span={8} style={{ paddingBottom: 20 }}>
        <Row style={{ display: "flex", alignItems: "center" }} gutter={24}>
          <Col span={3}>{props.icon}</Col>
          <Col span={21}>
            <div style={{ marginBottom: 8 }}>
              <small style={{ color: "#B4A4AD" }}>{props.title}</small>
            </div>
            <div style={{ color: "#746B6B", fontSize: 16, fontWeight: "bold", marginTop: -6 }}>{props.identity}</div>
          </Col>
        </Row>
      </Col>
    </>
  );
}

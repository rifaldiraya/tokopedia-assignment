import React, { useState } from "react";
import { Row, Col, Spin, Divider, Avatar, Image, Tooltip, Button, Modal, Input } from "antd";
import { CheckCircleOutlined, SortDescendingOutlined, DingdingOutlined, FieldNumberOutlined, DropboxOutlined, BlockOutlined } from "@ant-design/icons";
import { myPokemonList } from "../../data/MyPokemonData";
import { GET_POKEMON } from "../../query/queries";
import { useQuery } from "@apollo/client";
import Identity from "./Identity";

export default function PokemonDetail(props) {
  const { loading, data: pokemonDetail } = useQuery(GET_POKEMON, {
    variables: { id: props.id },
  });

  console.log(myPokemonList);
  const [setName] = useState();

  const catchPokemon = () => {
    var randomNumber = Math.floor(Math.random() * 10);
    if (randomNumber > 4) {
      const getName = (val) => {
        setName(val.target.value);
      };
      const tes = (val) => {
        myPokemonList.pokemon.push(pokemonDetail);
      };
      Modal.success({
        afterClose: tes,
        title: "Success",
        content: (
          <>
            <div>This Pokemon is yours, Give new name</div>
            <Input placeholder="Name" onChange={getName} />
          </>
        ),
      });
    } else {
      Modal.error({
        title: "Failed",
        content: "Pokemon failed to catch, try again",
      });
    }
  };

  const types = (type) => (
    <div>
      <Tooltip title={type} color="#36AE92">
        {type === "Fire" ? <Image preview={false} width={50} src="/Fire.svg" /> : <></>}
        {type === "Grass" ? <Image preview={false} width={50} src="/Grass.svg" /> : <></>}
        {type === "Poison" ? <Image preview={false} width={50} src="/poison.svg" /> : <></>}
        {type === "Flying" ? <Image preview={false} width={50} src="/windy.svg" /> : <></>}
        {type === "Water" ? <Image preview={false} width={50} src="/Water.svg" /> : <></>}
        {type === "Normal" ? <Image preview={false} width={50} src="/normal.svg" /> : <></>}
        {type === "Bug" ? <Image preview={false} width={50} src="/bug.svg" /> : <></>}
        {type === "Electric" ? <Image preview={false} width={50} src="/electric.svg" /> : <></>}
        {type === "Ground" ? <Image preview={false} width={50} src="/ground.svg" /> : <></>}
        {type === "Fairy" ? <Image preview={false} width={50} src="/fairy.svg" /> : <></>}
        {type === "Fighting" ? <Image preview={false} width={50} src="/fighting.svg" /> : <></>}
        {type === "Ice" ? <Image preview={false} width={50} src="/ice.svg" /> : <></>}
        {type === "Steel" ? <Image preview={false} width={50} src="/steel.svg" /> : <></>}
        {type === "Ghost" ? <Image preview={false} width={50} src="/ghost.svg" /> : <></>}
        {type === "Rock" ? <Image preview={false} width={50} src="/Stones.svg" /> : <></>}
        {type === "Psychic" ? <Image preview={false} width={50} src="/psychic.svg" /> : <></>}
        {type === "Dragon" ? <Image preview={false} width={50} src="/dragon.svg" /> : <></>}
        {type === "Dark" ? <Image preview={false} width={50} src="/dark.svg" /> : <></>}
      </Tooltip>
    </div>
  );

  return (
    <>
      <div className="bkl" style={{ paddingTop: "70px" }}>
        <div style={{ textAlign: "center" }}>
          <Spin style={{ paddingTop: 100 }} spinning={loading} />
        </div>
        {pokemonDetail && (
          <>
            <Row>
              <Col span={24} style={{ width: "100%", display: "flex", justifyContent: "center", alignItems: "center" }}>
                <Avatar size={180} src={pokemonDetail.pokemon.image} style={{ boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px", width: 180 }} />
              </Col>
              <br />
              <Col span={24} style={{ paddingTop: "25px" }}>
                <Row gutter={36} style={{ width: "100%", display: "flex", justifyContent: "center", alignItems: "center" }}>
                  {pokemonDetail.pokemon.types.map((item, index) => (
                    <Col style={{}}>{types(item)}</Col>
                  ))}
                </Row>
                <br />
                <Divider plain>
                  <div style={{ padding: "10px", borderRadius: "20px", backgroundColor: "#15957c" }}>
                    <span style={{ color: "white" }} className="title">
                      {pokemonDetail.pokemon.name}
                    </span>
                  </div>
                </Divider>
              </Col>
            </Row>
            <Row gutter={36} style={{ padding: "25px" }}>
              <Identity title="Name" identity={pokemonDetail.pokemon.name} icon={<CheckCircleOutlined style={{ color: "#36AE92", fontSize: 25 }} />} />
              <Identity title="Pokemon Number" identity={pokemonDetail.pokemon.number} icon={<FieldNumberOutlined style={{ color: "#36AE92", fontSize: 25 }} />} />
              <Identity title="Classification" identity={pokemonDetail.pokemon.classification} icon={<BlockOutlined style={{ color: "#36AE92", fontSize: 25 }} />} />
              <Identity
                title="Weight (Minimum-Maximum)"
                identity={`${pokemonDetail.pokemon.weight.minimum}-${pokemonDetail.pokemon.weight.maximum}`}
                icon={<SortDescendingOutlined style={{ color: "#36AE92", fontSize: 25 }} />}
              />
              <Identity
                title="Height (Minimum-Maximum)"
                identity={`${pokemonDetail.pokemon.height.minimum}-${pokemonDetail.pokemon.height.maximum}`}
                icon={<SortDescendingOutlined style={{ color: "#36AE92", fontSize: 25 }} />}
              />
              <Identity title="Flee Rate" identity={pokemonDetail.pokemon.fleeRate} icon={<DingdingOutlined style={{ color: "#36AE92", fontSize: 25 }} />} />
            </Row>
            <Divider>
              <div className="card-title" style={{ fontSize: 36, textAlign: "center" }}>
                Resistant
              </div>
            </Divider>
            <Row gutter={36} style={{ width: "100%", display: "flex", justifyContent: "center", alignItems: "center" }}>
              {pokemonDetail.pokemon.resistant.map((item, index) => (
                <Col style={{}}>{types(item)}</Col>
              ))}
            </Row>
            <Divider>
              <div className="card-title" style={{ fontSize: 36, textAlign: "center" }}>
                Weaknesses
              </div>
            </Divider>
            <Row gutter={36} style={{ width: "100%", display: "flex", justifyContent: "center", alignItems: "center" }}>
              {pokemonDetail.pokemon.weaknesses.map((item, index) => (
                <Col style={{}}>{types(item)}</Col>
              ))}
            </Row>
            <Divider>
              <div className="card-title" style={{ fontSize: 36, textAlign: "center" }}>
                Evolutions
              </div>
            </Divider>
            <Row gutter={36} style={{ width: "100%", display: "flex", justifyContent: "center", alignItems: "center" }}>
              {pokemonDetail &&
                pokemonDetail.pokemon &&
                pokemonDetail.pokemon.evolutions &&
                pokemonDetail.pokemon.evolutions.map((item, index) => (
                  <>
                    <Col
                      style={{
                        paddingRigth: 10,
                        fontSize: 12,
                        textAlign: "center",
                        backgroundColor: "rgb(40,175,163, 0.8)",
                        border: "2px solid #28AFA3",
                        padding: "3px 15px 3px 15px",
                        borderRadius: 6,
                      }}
                    >
                      <span style={{ color: "white" }} className="card-title">
                        {item.name} | {item.classification}
                      </span>
                    </Col>
                  </>
                ))}
            </Row>
            <Divider>
              <div className="card-title" style={{ fontSize: 36, textAlign: "center" }}>
                Special Attacks
              </div>
            </Divider>
            <Row gutter={36} style={{ width: "100%", display: "flex", justifyContent: "center", alignItems: "center" }}>
              {pokemonDetail &&
                pokemonDetail.pokemon &&
                pokemonDetail.pokemon.attacks &&
                pokemonDetail.pokemon.attacks.special.map((item, index) => (
                  <>
                    <Col
                      style={{
                        paddingRigth: 10,
                        fontSize: 12,
                        textAlign: "center",
                        backgroundColor: "rgb(40,175,163, 0.8)",
                        border: "2px solid #28AFA3",
                        padding: "3px 15px 3px 15px",
                        borderRadius: 6,
                      }}
                    >
                      <span style={{ color: "white" }} className="card-title">
                        {item.name} | {item.type} | {item.damage}
                      </span>
                    </Col>
                  </>
                ))}
            </Row>
            <Divider>
              <div className="card-title" style={{ fontSize: 36, textAlign: "center" }}>
                Fast Attacks
              </div>
            </Divider>
            <Row gutter={36} style={{ width: "100%", display: "flex", justifyContent: "center", alignItems: "center" }}>
              {pokemonDetail &&
                pokemonDetail.pokemon &&
                pokemonDetail.pokemon.attacks &&
                pokemonDetail.pokemon.attacks.fast.map((item, index) => (
                  <>
                    <Col
                      style={{
                        paddingRigth: 10,
                        fontSize: 12,
                        textAlign: "center",
                        backgroundColor: "rgb(40,175,163, 0.8)",
                        border: "2px solid #28AFA3",
                        padding: "3px 15px 3px 15px",
                        borderRadius: 6,
                      }}
                    >
                      <span style={{ color: "white" }} className="card-title">
                        {item.name} | {item.type} | {item.damage}
                      </span>
                    </Col>
                  </>
                ))}
            </Row>
            <Button type="primary" shape="circle" icon={<DropboxOutlined />} size="large" style={{ position: "fixed", right: 40, bottom: 50, width: 50, height: 50 }} onClick={catchPokemon} />
          </>
        )}
      </div>
    </>
  );
}

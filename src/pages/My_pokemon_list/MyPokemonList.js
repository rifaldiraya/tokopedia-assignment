import React from "react";
import AllProduct from "./AllPokemon";
import TitleComponent from "../../components/TitleComponent";

export default function MyPokemonList() {
  return (
    <>
      <div className="product">
        <TitleComponent
          tCol="#ffff"
          title="My Pokemon List"
          desc="Your Pokemon list, you can release and see your Pokemon details"
        />
      </div>
      <AllProduct />
    </>
  );
}

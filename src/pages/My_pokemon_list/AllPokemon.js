import React from "react";
import { Row, Col, Card, Badge, Rate, Spin } from "antd";
import { useQuery } from "@apollo/client";
import { GET_POKEMONS } from "../../query/queries";
import { myPokemonList } from "../../data/MyPokemonData";

export default function AllPokemon() {
  const { loading } = useQuery(GET_POKEMONS, {
    variables: { first: 50 },
  });

  const toDetails = (id) => {};

  return (
    <div className="bkl">
      <div style={{ textAlign: "center" }}>
        <Spin spinning={loading} />
      </div>

      <Row gutter={[24, 24]}>
        {myPokemonList &&
          myPokemonList.pokemon.map((item, index) => {
            return (
              <>
                {index === 0 ? (
                  <></>
                ) : (
                  <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <a href={`pokemon_detail/${item.pokemon.id}`}>
                      {item.pokemon.evolutions ? (
                        <Badge.Ribbon color="#15957c" text={`Has ${item.pokemon.evolutions.length} Evolutions`}>
                          <Card
                            hoverable
                            style={{ borderRadius: "20px", width: "auto", minHeight: "400px" }}
                            cover={<img style={{ maxWidth: 200, maxHeight: 200, borderTopLeftRadius: "20px", borderTopRightRadius: "20px" }} alt={item.pokemon.name} src={item.pokemon.image} />}
                            onClick={() => toDetails(item.pokemon.id)}
                          >
                            <div className="card-title" style={{ minHeight: "50px", paddingBottom: "10px", textAlign: "center" }}>
                              <span>{item.pokemon.name}</span>
                            </div>
                            <div>
                              <Row>
                                <Col style={{ textAlign: "left" }} span={12}>
                                  <span style={{ fontSize: "14px", color: "#3d3d3d", fontWeight: "bold" }} className="card-description">
                                    HP: {item.pokemon.maxHP}
                                    <br />
                                    CP: {item.pokemon.maxCP}
                                  </span>
                                </Col>
                                <Col style={{ textAlign: "right" }} span={12}>
                                  <span className="card-description">{item.pokemon.maxHP} Owned</span>
                                </Col>
                              </Row>
                            </div>
                            <br />
                            <Row style={{ bottom: "0" }}>
                              <Col style={{ textAlign: "left" }} span={12}>
                                <Rate disabled defaultValue={item.pokemon.attacks && item.pokemon.attacks.special.length} style={{ fontSize: "12px" }} />
                              </Col>
                              <Col style={{ textAlign: "right" }} span={12}>
                                <span style={{ textTransform: "uppercase", color: "#3d3d3d" }} className="card-description">
                                  {item.pokemon.name}
                                </span>
                              </Col>
                            </Row>
                          </Card>
                        </Badge.Ribbon>
                      ) : (
                        <Card
                          hoverable
                          style={{ borderRadius: "20px", width: "auto", minHeight: "400px" }}
                          cover={<img style={{ maxWidth: 200, maxHeight: 200, borderTopLeftRadius: "20px", borderTopRightRadius: "20px" }} alt={item.pokemon.name} src={item.pokemon.image} />}
                        >
                          <div className="card-title" style={{ minHeight: "50px", paddingBottom: "10px", textAlign: "center" }}>
                            <span>{item.pokemon.name}</span>
                          </div>
                          <div>
                            <Row>
                              <Col style={{ textAlign: "left" }} span={12}>
                                <span style={{ fontSize: "14px", color: "#3d3d3d", fontWeight: "bold" }} className="card-description">
                                  HP: {item.pokemon.maxHP}
                                  <br />
                                  CP: {item.pokemon.maxCP}
                                </span>
                              </Col>
                              <Col style={{ textAlign: "right" }} span={12}>
                                <span className="card-description">{item.pokemon.maxHP} Owned</span>
                              </Col>
                            </Row>
                          </div>
                          <br />
                          <Row style={{ bottom: "0" }}>
                            <Col style={{ textAlign: "left" }} span={12}>
                              {/* <Rate disabled defaultValue={item.rating} style={{ fontSize: "12px" }} /> */}
                            </Col>
                            <Col style={{ textAlign: "right" }} span={12}>
                              <span style={{ textTransform: "uppercase", color: "#3d3d3d" }} className="card-description">
                                {item.pokemon.name}
                              </span>
                            </Col>
                          </Row>
                        </Card>
                      )}
                    </a>
                  </Col>
                )}
              </>
            );
          })}
      </Row>
    </div>
  );
}

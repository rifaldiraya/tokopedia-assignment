import React from "react";
import { Row, Col, Card, Badge, Rate, Spin } from "antd";
import { useQuery } from "@apollo/client";
import { GET_POKEMONS } from "../../query/queries";

export default function PokemonList() {
  const { loading, data: pokemonList } = useQuery(GET_POKEMONS, {
    variables: { first: 50 },
  });

  const toDetails = (id) => {};

  return (
    <div style={{ padding: "10px 5px 10px 5px" }}>
      <div style={{ textAlign: "center" }}>
        <Spin spinning={loading} />
      </div>

      <Row gutter={[24, 24]}>
        {pokemonList &&
          pokemonList.pokemons.map((item) => {
            return (
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <a href={`pokemon_detail/${item.id}`}>
                  {item.evolutions ? (
                    <Badge.Ribbon color="#15957c" text={`Has ${item.evolutions.length} Evolutions`}>
                      <Card
                        hoverable
                        style={{ borderRadius: "20px", width: "auto", minHeight: "400px" }}
                        cover={<img style={{ maxWidth: 200, maxHeight: 200, borderTopLeftRadius: "20px", borderTopRightRadius: "20px" }} alt={item.name} src={item.image} />}
                        onClick={() => toDetails(item.id)}
                      >
                        <div className="card-title" style={{ minHeight: "50px", paddingBottom: "10px", textAlign: "center" }}>
                          <span>{item.name}</span>
                        </div>
                        <div>
                          <Row>
                            <Col style={{ textAlign: "left" }} span={12}>
                              <span style={{ fontSize: "14px", color: "#3d3d3d", fontWeight: "bold" }} className="card-description">
                                HP: {item.maxHP}
                                <br />
                                CP: {item.maxCP}
                              </span>
                            </Col>
                            <Col style={{ textAlign: "right" }} span={12}>
                              <span className="card-description">{item.maxHP} Owned</span>
                            </Col>
                          </Row>
                        </div>
                        <br />
                        <Row style={{ bottom: "0" }}>
                          <Col style={{ textAlign: "left" }} span={12}>
                            <Rate disabled defaultValue={item.attacks && item.attacks.special.length} style={{ fontSize: "12px" }} />
                          </Col>
                          <Col style={{ textAlign: "right" }} span={12}>
                            <span style={{ textTransform: "uppercase", color: "#3d3d3d" }} className="card-description">
                              {item.name}
                            </span>
                          </Col>
                        </Row>
                      </Card>
                    </Badge.Ribbon>
                  ) : (
                    <Card
                      hoverable
                      style={{ borderRadius: "20px", width: "auto", minHeight: "400px" }}
                      cover={<img style={{ maxWidth: 200, maxHeight: 200, borderTopLeftRadius: "20px", borderTopRightRadius: "20px" }} alt={item.name} src={item.image} />}
                    >
                      <div className="card-title" style={{ minHeight: "50px", paddingBottom: "10px", textAlign: "center" }}>
                        <span>{item.name}</span>
                      </div>
                      <div>
                        <Row>
                          <Col style={{ textAlign: "left" }} span={12}>
                            <span style={{ fontSize: "14px", color: "#3d3d3d", fontWeight: "bold" }} className="card-description">
                              HP: {item.maxHP}
                              <br />
                              CP: {item.maxCP}
                            </span>
                          </Col>
                          <Col style={{ textAlign: "right" }} span={12}>
                            <span className="card-description">{item.maxHP} Owned</span>
                          </Col>
                        </Row>
                      </div>
                      <br />
                      <Row style={{ bottom: "0" }}>
                        <Col style={{ textAlign: "left" }} span={12}>
                          {/* <Rate disabled defaultValue={item.rating} style={{ fontSize: "12px" }} /> */}
                        </Col>
                        <Col style={{ textAlign: "right" }} span={12}>
                          <span style={{ textTransform: "uppercase", color: "#3d3d3d" }} className="card-description">
                            {item.name}
                          </span>
                        </Col>
                      </Row>
                    </Card>
                  )}
                </a>
              </Col>
            );
          })}
      </Row>
    </div>
  );
}

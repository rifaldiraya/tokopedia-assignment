import React from "react";
import { Row, Col, Image } from "antd";

export default function Hero() {
  return (
    <>
      <div className="hero">
        <Row>
          <Col style={{ padding: "100px 0px 0px 0px" }} xs={24} sm={24} md={24} lg={12} xl={12}>
            <div className="title">
              <span style={{ color: "white", fontWeight: "bold", fontSize: "48px" }}>Catch your Pokemon</span>
            </div>
            <div className="description" style={{ color: "white" }}>
              <span style={{ fontSize: "24px" }}>Get Eternal Pokemon and Battle With Friends. You can find hundreds of Pokemon around you, each Pokemon has its own race, Catch it Now</span>
            </div>
          </Col>
          <Col style={{ textAlign: "center" }} xs={24} sm={24} md={24} lg={12} xl={12}>
            <Image src="hero.svg" />
          </Col>
        </Row>
      </div>
    </>
  );
}

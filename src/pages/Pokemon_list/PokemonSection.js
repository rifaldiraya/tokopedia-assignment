import React from "react";
import { Divider } from "antd";
import PokemonList from "./PokemonList";

export default function PokemonSection() {
  return (
    <div style={{ paddingTop: "10px" }}>
      <Divider plain>
        <div style={{ padding: "10px", borderRadius: "20px", backgroundColor: "#15957c" }}>
          <span style={{ color: "white" }} className="title">
            Pokemon That You Can Catch
          </span>
        </div>
      </Divider>
      <PokemonList />
    </div>
  );
}

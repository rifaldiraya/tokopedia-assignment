import React from "react";
import Hero from "./Hero";
import PokemonSection from "./PokemonSection";

export default function Beranda() {
  return (
    <div>
      <Hero />
      <div className="bkl">
        <PokemonSection />
      </div>
    </div>
  );
}

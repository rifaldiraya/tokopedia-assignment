import React from "react";
import { Link } from "react-router-dom";
import * as routes from "../config/Routes";

export default function RightMenu() {
  return (
    <>
      <div className="rightMenu">
        <Link style={{ color: "#36AE92", paddingRight: "20px" }} to={routes.MY_POKEMON_LIST}>
          <span>My Pokemon List</span>
        </Link>
      </div>
    </>
  );
}

import React from "react";
import { Layout, Row, Col } from "antd";

const { Footer } = Layout;

export default function MainFooter() {
  return (
    <>
      <div>
        <Footer preview={false} className="main-footer">
          <Row gutter={[16, 16]} style={{ fontSize: "16px" }}>
            <Col xs={24} sm={24} md={20} lg={20} xl={20}>
              <div>
                <span style={{ textAlign: "center" }}>Copyright © 2022 Pokemon Go </span>
              </div>
            </Col>
          </Row>
        </Footer>
      </div>
    </>
  );
}

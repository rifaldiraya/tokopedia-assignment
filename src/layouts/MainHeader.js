import React, { useState } from "react";
import { Layout, Space, Drawer } from "antd";
import { Link } from "react-router-dom";
import * as routes from "../config/Routes";
import { MenuOutlined, HomeOutlined } from "@ant-design/icons";
import RightMenu from "./RightMenu";

const { Header } = Layout;
// const { Search } = Input;
// const onSearch = (value) => console.log(value);

export default function MainHeader() {
  const [visible, setVisible] = useState(false);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  return (
    <>
      <Layout>
        <Header style={{ position: "fixed", zIndex: 1, width: "100%", color: "black", backgroundColor: "white" }}>
          <Space>
            <Link to={routes.POKEMON_LIST}>
              <span style={{ fontWeight: "bold", textAlign: "center", color: "#36AE92" }}>Pokemon Go</span>
            </Link>
            {/* <div style={{ padding: "8px 0px 0px 30px" }}>
              <Search placeholder="Cari disini..." style={{ width: "25vw" }} onSearch={onSearch} enterButton />
            </div> */}
          </Space>
          <div className="rightMenu">
            <RightMenu />
          </div>
          <div style={{ float: "right", height: "100%", display: "flex", justifyContent: "center", alignItems: "center" }}>
            <MenuOutlined style={{ fontSize: "16px", color: "#36AE92" }} className="barsMenu" onClick={showDrawer} />
          </div>
        </Header>
      </Layout>
      <Drawer style={{ fontWeight: "bold", textAlign: "center" }} height="80" placement="bottom" onClose={onClose} visible={visible} closable={false}>
        <Space>
          <Link style={{ paddingRight: "10px", textAlign: "center", color: "#36AE92" }} to={routes.POKEMON_LIST}>
            <Space direction="vertical">
              <HomeOutlined style={{ fontWeight: "bold", fontSize: "20px" }} />
              <span>My Pokemon List</span>
            </Space>
          </Link>
        </Space>
      </Drawer>
    </>
  );
}

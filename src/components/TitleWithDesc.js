import React from "react";

export default function TitleWithDesc(props) {
  return (
    <>
      <div style={{ width: "100%", justifyContent: "center", paddingBottom: "50px", alignItems: "center", textAlign: "center" }}>
        <span className="title">{props.title}</span>
        <br />
        <span style={{ fontWeight: "bold" }} className="desc">
          {props.desc}
        </span>
      </div>
    </>
  );
}

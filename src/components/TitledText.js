import React from "react";

export default function TitledText(props) {
  return (
    <>
      <div className="title" style={{ width: "100%", display: "flex", justifyContent: "center", alignItems: "center", paddingBottom: "50px", textAlign: "center" }}>
        <span>{props.title}</span>
      </div>
    </>
  );
}

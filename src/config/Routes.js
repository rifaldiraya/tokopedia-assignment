export const LANDING = "/";
export const POKEMON_LIST = "/pokemon_list";
export const POKEMON_DETAIL = "/pokemon_detail/:id";
export const MY_POKEMON_LIST = "/my-pokemon-list";

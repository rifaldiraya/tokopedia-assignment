import React from "react";
import { Route, Switch } from "react-router-dom";
import PokemonList from "../pages/Pokemon_list/Beranda";
import PokemonDetail from "../pages/Pokemon_details/PokemonDetail";
import Product from "../pages/My_pokemon_list/MyPokemonList";
import * as routes from "./Routes";

const pokemonId = (match) => {
  return <PokemonDetail id={match.match.params.id} />;
};

export default function RoutePages(props) {
  return (
    <Switch>
      <Route exact path={routes.LANDING} component={PokemonList} />
      <Route exact path={routes.POKEMON_LIST} component={PokemonList} />
      <Route exact path={routes.POKEMON_DETAIL} component={pokemonId} />
      <Route exact path={routes.MY_POKEMON_LIST} component={Product} />
    </Switch>
  );
}

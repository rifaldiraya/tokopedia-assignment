export const myPokemonList = {
  pokemon: [
    {
      name: "My First Pokemon",
      number: "007",
      weight: {
        minimum: "7.88kg",
        maximum: "10.13kg",
      },
      height: {
        minimum: "0.44m",
        maximum: "0.56m",
      },
      types: ["Water"],
      maxHP: 1008,
      resistant: ["Fire", "Water", "Ice", "Steel"],
      maxCP: 891,
      classification: "Tiny Turtle Pokémon",
      fleeRate: 0.1,
      weaknesses: ["Electric", "Grass"],
      image: "https://img.pokemondb.net/artwork/squirtle.jpg",
      evolutions: [
        {
          name: "Wartortle",
          classification: "Turtle Pokémon",
        },
        {
          name: "Blastoise",
          classification: "Shellfish Pokémon",
        },
      ],
      attacks: {
        special: [
          {
            name: "Aqua Jet",
            type: "Water",
            damage: 25,
          },
          {
            name: "Aqua Tail",
            type: "Water",
            damage: 45,
          },
          {
            name: "Water Pulse",
            type: "Water",
            damage: 35,
          },
        ],
        fast: [
          {
            name: "Bubble",
            type: "Water",
            damage: 25,
          },
          {
            name: "Tackle",
            type: "Normal",
            damage: 12,
          },
        ],
      },
    },
  ],
};
